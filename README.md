Extension_Tool for Unity
====

Unity用の拡張ツールを梱包しています。

* CustomEditor - エディター拡張
  * [SceneNameListPulldown](https://gitlab.com/consolesoup/extension_tool-for-unity/tree/master/Assets/CustomEditor/SceneNameListPulldown#scenenamelistpulldown) - Sceneの名前を選択できるプルダウンメニュー
  * [TagNameListPulldown](https://gitlab.com/consolesoup/extension_tool-for-unity/tree/master/Assets/CustomEditor/TagNameListPulldown#tagnamelistpulldown) - タグを選択できるプルダウンメニュー
* Network
  * [ProxyNetwork](https://gitlab.com/consolesoup/extension_tool-for-unity/tree/master/Assets/Network/ProxyNetwork#proxynetwork) - ログイン機能有りのProxy通信クラス
* Payment
  * [EazyPayment](https://gitlab.com/consolesoup/extension_tool-for-unity/tree/master/Assets/Payment/EazyPayment#eazypayment) - Android, iOS用の課金用クラス(UnityEditorでもデモで動く)

各ツールの利用方法や利用規約は、上のリンクから各READMEをご覧ください。

- - -

## Licence

The MIT License (MIT)
Copyright (c) 2019 ConsoleSoup

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

- - -

## Author

[ConsoleSoup](https://twitter.com/consolesoup)

[うさぎのとおりみち](https://twitter.com/rabitroad)