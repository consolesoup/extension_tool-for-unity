ProxyNetwork
====

.NetのHttpWebRequestを利用したProxyを利用したGETとPOSTができる通信クラスです。

* 業務実用経験あり
* 48時間以上の耐久テスト検証済み（Unity 2017.4.15f1）

[Download (unitypackage)](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/master/Assets/Network/ProxyNetwork/ProxyNetwork.unitypackage)

- - -

## Description

* **内容物**  
  * ProxyNetwork.cs (必須)
  * README.md
  * Sample
    * Sample_ProxyNetwork.unity
    * Sample_ProxyNetwork.cs
  * ProxyNetwork.unitypackage

* **使用方法**  

**宣言**  
usingでNetworkの利用を宣言する。
``` csharp
using RabitRoad.Network;
```
<br>

**Proxy情報を生成**  
stringの値はProxyのログイン情報を入れてください。  

|変数|説明|必須|
|:---|:---|:---|
|SERVER|Proxyサーバーのアドレス|◯|
|PORT|Proxyサーバーの利用ポート|◯|
|USER|Proxyサーバーのログインユーザー||
|PASS|Proxyサーバーのログインパスワード||

``` csharp
    string SERVER, PORT, USER, PASS;
    
    System.Net.WebProxy proxy;
    if (!string.IsNullOrEmpty(SERVER) && !string.IsNullOrEmpty(PORT))
    {
        proxy = new System.Net.WebProxy(SERVER + ":" + PORT);
        if (!string.IsNullOrEmpty(USER) && !string.IsNullOrEmpty(PASS)) proxy.Credentials = new System.Net.NetworkCredential(USER, PASS);
    }
    else proxy = null;
```
<br>

**GET通信**  
URLには通信先のパスを入れてください。  

デフォルトはバイナリデータとして返ってきます。

``` csharp
    string URL;

    System.Net.WebProxy proxy; /*上で作ったProxy情報*/
    if (proxy != null) ProxyNetwork.Proxy = proxy;

    ProxyNetwork.Get(URL, (byte[] bytes, string error) => {
        //成功の場合は error = null
    });
```
<br>
コールバックの戻り値をstringにすることでUTF8でデコードした文字列を受け取ることも可能。

``` csharp
    string URL;

    System.Net.WebProxy proxy; /*上で作ったProxy情報*/
    if (proxy != null) ProxyNetwork.Proxy = proxy;

    ProxyNetwork.Get(URL, (string json, string error) => {
        //成功の場合は error = null
    });
```

<br>
UTF8だと文字化けする場合は文字コードを指定してください。

``` csharp
    string URL;

    System.Net.WebProxy proxy; /*上で作ったProxy情報*/
    if (proxy != null) ProxyNetwork.Proxy = proxy;

    ProxyNetwork.Get(URL, System.Text.Encoding.UTF8, (string json, string error) => {
        //成功の場合は error = null
    });
```

<br>
戻り値をAssetBundleにすることも可能。

バイナリデータをAssetBundle.LoadFromMemoryで変換しているだけです。  
一応キャッシュしていますが、自分でAssetBundleの管理をしたい場合はbyte[]で受け取ってください。

``` csharp
    string URL;

    System.Net.WebProxy proxy; /*上で作ったProxy情報*/
    if (proxy != null) ProxyNetwork.Proxy = proxy;

    ProxyNetwork.Get(URL, (AssetBundle asset, string error) => {
        //成功の場合は error = null
    });
```

<br>
※使用例はSampleをご覧ください

* **注意事項**
1. テスト用のProxyサーバーなどがないのでSampleは動かないです。あくまで記述例として参考にどうぞ。

- - -

## Terms

1. 当データを利用した時点で規約に同意したものとみなします。
2. 当データを改変・改造したデータにも当規約が適用されるものとします。
3. 改造したデータの著作権は当方が所持するものとします。

4. 利用可能
  * 営利利用
  * 改変・改造
  * R-18，R-18G

5. 禁止事項
  * 当データ単体での再配布、販売を禁止します（改造品も含まれます）。
  * ほかの利用者への配布による譲渡を禁止します。（会社、団体の場合はチーム内共有を許可）
  * 自作発言を禁止します（改造品も含まれます）。
  * 公序良俗に反する行為や目的、政治, 宗教活動、他者に対する誹謗中傷目的での利用を禁止します。

6. 免責事項
  * 当データについて当方はいかなる保証も行いません。
  * 当データを利用して何らかのトラブル・損失及び損害等が発生した場合でも当方は一切の責任を負いません。

- - -

## Licence

配布サイトの最新のものを適用する。