using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Net;

namespace RabitRoad.Network
{
    public class ProxyNetwork
    {
        private static WebProxy _proxy = null;
        public static WebProxy Proxy
        {
            get { return _proxy; }
            set { _proxy = value; }
        }

        private static Dictionary<string, AssetBundle> _assetBundleDict = new Dictionary<string, AssetBundle>();

        public enum ContentType
        {
            Application_x_www_form_urlencoded,
            Application_json
        }
        private static string _contentTypeString(ContentType type)
        {
            string contentType = "";
            switch (type)
            {
                case ContentType.Application_x_www_form_urlencoded:
                    contentType = "application/x-www-form-urlencoded";
                    break;
                case ContentType.Application_json:
                    contentType = "application/json";
                    break;
            }
            return contentType;
        }

        private static long _codeFromHttpStatusCode(HttpStatusCode StatusCode)
        {
            long code = 200;
            switch (StatusCode)
            {
                case HttpStatusCode.Continue:
                    code = 100;
                    break;
                case HttpStatusCode.SwitchingProtocols:
                    code = 101;
                    break;
                case HttpStatusCode.OK:
                    code = 200;
                    break;
                case HttpStatusCode.Created:
                    code = 201;
                    break;
                case HttpStatusCode.Accepted:
                    code = 202;
                    break;
                case HttpStatusCode.NonAuthoritativeInformation:
                    code = 203;
                    break;
                case HttpStatusCode.NoContent:
                    code = 204;
                    break;
                case HttpStatusCode.ResetContent:
                    code = 205;
                    break;
                case HttpStatusCode.PartialContent:
                    code = 206;
                    break;
                case HttpStatusCode.MultipleChoices:
                    code = 300;
                    break;
                case HttpStatusCode.MovedPermanently:
                    code = 301;
                    break;
                case HttpStatusCode.Found:
                    code = 302;
                    break;
                case HttpStatusCode.SeeOther:
                    code = 303;
                    break;
                case HttpStatusCode.NotModified:
                    code = 304;
                    break;
                case HttpStatusCode.UseProxy:
                    code = 305;
                    break;
                case HttpStatusCode.Unused:
                    code = 306;
                    break;
                case HttpStatusCode.TemporaryRedirect:
                    code = 307;
                    break;
                case HttpStatusCode.BadRequest:
                    code = 400;
                    break;
                case HttpStatusCode.Unauthorized:
                    code = 401;
                    break;
                case HttpStatusCode.PaymentRequired:
                    code = 402;
                    break;
                case HttpStatusCode.Forbidden:
                    code = 403;
                    break;
                case HttpStatusCode.NotFound:
                    code = 404;
                    break;
                case HttpStatusCode.MethodNotAllowed:
                    code = 405;
                    break;
                case HttpStatusCode.NotAcceptable:
                    code = 406;
                    break;
                case HttpStatusCode.ProxyAuthenticationRequired:
                    code = 407;
                    break;
                case HttpStatusCode.RequestTimeout:
                    code = 408;
                    break;
                case HttpStatusCode.Conflict:
                    code = 409;
                    break;
                case HttpStatusCode.Gone:
                    code = 410;
                    break;
                case HttpStatusCode.LengthRequired:
                    code = 411;
                    break;
                case HttpStatusCode.PreconditionFailed:
                    code = 412;
                    break;
                case HttpStatusCode.RequestEntityTooLarge:
                    code = 413;
                    break;
                case HttpStatusCode.RequestUriTooLong:
                    code = 414;
                    break;
                case HttpStatusCode.UnsupportedMediaType:
                    code = 415;
                    break;
                case HttpStatusCode.RequestedRangeNotSatisfiable:
                    code = 416;
                    break;
                case HttpStatusCode.ExpectationFailed:
                    code = 417;
                    break;
                case HttpStatusCode.InternalServerError:
                    code = 500;
                    break;
                case HttpStatusCode.NotImplemented:
                    code = 501;
                    break;
                case HttpStatusCode.BadGateway:
                    code = 502;
                    break;
                case HttpStatusCode.ServiceUnavailable:
                    code = 503;
                    break;
                case HttpStatusCode.GatewayTimeout:
                    code = 504;
                    break;
                case HttpStatusCode.HttpVersionNotSupported:
                    code = 505;
                    break;
            }
            return code;
        }

        //エラー内容は適切なものに変更してください。
        private static string _errorFromCode(HttpStatusCode StatusCode)
        {
            string error = null;
            switch (StatusCode)
            {
                case HttpStatusCode.Continue:
                    error = null;
                    break;
                case HttpStatusCode.SwitchingProtocols:
                    error = null;
                    break;
                case HttpStatusCode.OK:
                    error = null;
                    break;
                case HttpStatusCode.Created:
                    error = null;
                    break;
                case HttpStatusCode.Accepted:
                    error = null;
                    break;
                case HttpStatusCode.NonAuthoritativeInformation:
                    error = null;
                    break;
                case HttpStatusCode.NoContent:
                    error = null;
                    break;
                case HttpStatusCode.ResetContent:
                    error = null;
                    break;
                case HttpStatusCode.PartialContent:
                    error = null;
                    break;
                case HttpStatusCode.MultipleChoices:
                    error = "There are multiple requested resources.";
                    break;
                case HttpStatusCode.MovedPermanently:
                    error = "The requested resource has been moved permanently.";
                    break;
                case HttpStatusCode.Found:
                    error = "The requested resource has been temporarily moved.";
                    break;
                case HttpStatusCode.SeeOther:
                    error = "The response to the request exists in another URL.";
                    break;
                case HttpStatusCode.NotModified:
                    error = "The requested resource has not been updated.";
                    break;
                case HttpStatusCode.UseProxy:
                    error = "Request using the proxy indicated in the Location: header of the response.";
                    break;
                case HttpStatusCode.Unused:
                    error = "Unknown error.";
                    break;
                case HttpStatusCode.TemporaryRedirect:
                    error = "Do not change the request method.";
                    break;
                case HttpStatusCode.BadRequest:
                    error = "Bad request.";
                    break;
                case HttpStatusCode.Unauthorized:
                    error = "Requires authentication.";
                    break;
                case HttpStatusCode.PaymentRequired:
                    error = "Need to pay.";
                    break;
                case HttpStatusCode.Forbidden:
                    error = "Denied access to a resource.";
                    break;
                case HttpStatusCode.NotFound:
                    error = "Resource not found.";
                    break;
                case HttpStatusCode.MethodNotAllowed:
                    error = "Trying to use a method that is not allowed.";
                    break;
                case HttpStatusCode.NotAcceptable:
                    error = "Could not accept.";
                    break;
                case HttpStatusCode.ProxyAuthenticationRequired:
                    error = "Proxy authentication is required.";
                    break;
                case HttpStatusCode.RequestTimeout:
                    error = "Request did not complete in time.";
                    break;
                case HttpStatusCode.Conflict:
                    error = "Can not complete because it conflicts with current resources";
                    break;
                case HttpStatusCode.Gone:
                    error = "Resources moved and disappeared permanently. Do not know where went.";
                    break;
                case HttpStatusCode.LengthRequired:
                    error = "No Content-Length header.";
                    break;
                case HttpStatusCode.PreconditionFailed:
                    error = "Rejected because it failed in the precondition.";
                    break;
                case HttpStatusCode.RequestEntityTooLarge:
                    error = "Payload is too big. Request entity has exceeded the server's tolerance.";
                    break;
                case HttpStatusCode.RequestUriTooLong:
                    error = "Rejected processing because the URI is too long.";
                    break;
                case HttpStatusCode.UnsupportedMediaType:
                    error = "Specified media type is not supported.";
                    break;
                case HttpStatusCode.RequestedRangeNotSatisfiable:
                    error = "Requested data exceeding Resource size.";
                    break;
                case HttpStatusCode.ExpectationFailed:
                    error = "Expansion by Expect header fails. Extension can not respond.";
                    break;
                case HttpStatusCode.InternalServerError:
                    error = "Error occurred inside the server.";
                    break;
                case HttpStatusCode.NotImplemented:
                    error = "Used a method that is not implemented.";
                    break;
                case HttpStatusCode.BadGateway:
                    error = "The gateway proxy server received an invalid request and rejected it.";
                    break;
                case HttpStatusCode.ServiceUnavailable:
                    error = "The service is temporarily unavailable due to overload or maintenance.";
                    break;
                case HttpStatusCode.GatewayTimeout:
                    error = "The gateway proxy server timed out without an appropriate response from the server inferred from the URI.";
                    break;
                case HttpStatusCode.HttpVersionNotSupported:
                    error = "Request is an unsupported HTTP version.";
                    break;
                default:
                    error = "Unknown error.";
                    break;
            }
            return error;
        }

        //POST通信 String or Byte
        public static void Post(string url, ContentType contentType, string postData, System.Action<byte[], string> callback)
        {
            Post(url, contentType, System.Text.Encoding.UTF8, postData, callback);
        }
        public static void Post(string url, ContentType contentType, System.Text.Encoding encoding, string postData, System.Action<byte[], string> callback)
        {
            byte[] bytes = null;
            string error = null;
            try
            {
                //WebRequestの作成
                System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                if (_proxy != null) req.Proxy = _proxy;
                //メソッドにPOSTを指定
                req.Method = "POST";
                //ContentTypeを設定
                req.ContentType = _contentTypeString(contentType);

                //POST送信するデータを作成
                byte[] data = encoding.GetBytes(postData);

                //POST送信するデータの長さを指定
                req.ContentLength = data.Length;

                //データをPOST送信するためのStreamを取得
                using (System.IO.Stream reqStream = req.GetRequestStream())
                {
                    //送信するデータを書き込む
                    reqStream.Write(data, 0, data.Length);
                }

                //サーバーからの応答を受信するためのWebResponseを取得
                HttpStatusCode code = HttpStatusCode.OK;
                using (System.Net.HttpWebResponse res = (System.Net.HttpWebResponse)req.GetResponse())
                {
                    code = res.StatusCode;
                    using (System.IO.Stream st = res.GetResponseStream())
                    {

                        byte[] buf = new byte[32768]; // 一時バッファ

                        using (MemoryStream ms = new MemoryStream())
                        {
                            while (true)
                            {
                                // ストリームから一時バッファに読み込む
                                int read = st.Read(buf, 0, buf.Length);
                                if (read > 0)
                                {
                                    // 一時バッファの内容をメモリ・ストリームに書き込む
                                    ms.Write(buf, 0, read);
                                }
                                else break;
                            }
                            // メモリ・ストリームの内容をバイト配列に格納
                            bytes = ms.ToArray();
                        }
                    }
                }

                error = _errorFromCode(code);
                if (!string.IsNullOrEmpty(error)) error = code.ToString()+" ("+_codeFromHttpStatusCode(code)+"): "+error;
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            callback(bytes, error);
        }
        public static void Post(string url, ContentType contentType, string postData, System.Action<string, string> callback)
        {
            Post(url, contentType, System.Text.Encoding.UTF8, postData, callback);
        }
        public static void Post(string url, ContentType contentType, System.Text.Encoding encoding, string postData, System.Action<string, string> callback)
        {
            Post(url, contentType, postData, (byte[] bytes, string error) =>
            {
                string json = null;
                try
                {
                    json = encoding.GetString(bytes);
                }
                catch (Exception e)
                {
                    string _error = e.Message;
                    if (string.IsNullOrEmpty(error)) error = _error;
                    else error += "\n\n" + _error;
                }
                callback(json, error);
            });
        }

        //GET通信 String or Byte
        public static void Get(string url, System.Action<byte[], string> callback)
        {
            byte[] bytes = null;
            string error = null;
            try
            {
                //WebRequestの作成
                System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                if (_proxy != null) req.Proxy = _proxy;
                //メソッドにPOSTを指定
                req.Method = "GET";

                //サーバーからの応答を受信するためのWebResponseを取得
                HttpStatusCode code = HttpStatusCode.OK;
                using (System.Net.HttpWebResponse res = (System.Net.HttpWebResponse)req.GetResponse())
                {
                    code = res.StatusCode;
                    using (System.IO.Stream st = res.GetResponseStream())
                    {

                        byte[] buf = new byte[32768]; // 一時バッファ

                        using (MemoryStream ms = new MemoryStream())
                        {
                            while (true)
                            {
                                // ストリームから一時バッファに読み込む
                                int read = st.Read(buf, 0, buf.Length);
                                if (read > 0)
                                {
                                    // 一時バッファの内容をメモリ・ストリームに書き込む
                                    ms.Write(buf, 0, read);
                                }
                                else break;
                            }
                            // メモリ・ストリームの内容をバイト配列に格納
                            bytes = ms.ToArray();
                        }
                    }
                }
                error = _errorFromCode(code);
                if (!string.IsNullOrEmpty(error)) error = code.ToString()+" ("+_codeFromHttpStatusCode(code)+"): "+error;
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            callback(bytes, error);
        }
        public static void Get(string url, System.Action<string, string> callback)
        {
            Get(url, System.Text.Encoding.UTF8, callback);
        }
        public static void Get(string url, System.Text.Encoding encoding, System.Action<string, string> callback)
        {
            Get(url, (byte[] bytes, string error) =>
            {
                string json = null;
                try
                {
                    json = encoding.GetString(bytes);
                }
                catch (Exception e)
                {
                    string _error = e.Message;
                    if (string.IsNullOrEmpty(error)) error = _error;
                    else error += "\n\n" + _error;
                }
                callback(json, error);
            });
        }
        public static void Get(string url, System.Action<AssetBundle, string> callback)
        {
            AssetBundle asset = null;
            if (_assetBundleDict.ContainsKey(url)) asset = _assetBundleDict[url];

            if (asset != null) callback(asset, null);
            else
            {
                Get(url, (byte[] bytes, string error) =>
                {
                    try
                    {
                        asset = AssetBundle.LoadFromMemory(bytes);
                        _assetBundleDict.Add(url, asset);
                    }
                    catch (Exception e)
                    {
                        string _error = e.Message;
                        if (string.IsNullOrEmpty(error)) error = _error;
                        else error += "\n\n" + _error;
                    }
                    callback(asset, error);
                });
            }
        }
    }
}
