using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RabitRoad.Network;

public class Sample_ProxyNetwork : MonoBehaviour
{
    [SerializeField] private bool _useProxy;
    [SerializeField] private string _url, _server, _port, _user, _pass;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private System.Net.WebProxy _getWebProxy()
    {
        System.Net.WebProxy _proxy;
        if (_useProxy && !string.IsNullOrEmpty(_server) && !string.IsNullOrEmpty(_port))
        {
            _proxy = new System.Net.WebProxy(_server + ":" + _port);
            if (!string.IsNullOrEmpty(_user) && !string.IsNullOrEmpty(_pass)) _proxy.Credentials = new System.Net.NetworkCredential(_user, _pass);
        }
        else _proxy = null;

        return _proxy;
    }

    public void ProxyGET()
    {
        System.Net.WebProxy _proxy = _getWebProxy();
        if (_proxy != null) ProxyNetwork.Proxy = _proxy;

#if SAMPLE
        ProxyNetwork.Get(_url, (byte[] bytes, string error) => {
        　　・UnityActionの戻り値を変更可能
        　　　　・Texture or Sprite -> byte[]
        　　　　・string -> string
        　　　　・AssetBundle -> AssetBundle //内部キャッシュ有り

        ProxyNetwork.Get(_url, System.Text.Encoding.UTF8, (string json, string error) => {
        　　・エンコードタイプ指定可（default = UTF8）
#endif
        ProxyNetwork.Get(_url, (string json, string error) => {
            //成功の場合は error = null
        });
    }

    public void ProxyPOST()
    {
        System.Net.WebProxy _proxy = _getWebProxy();
        if (_proxy != null) ProxyNetwork.Proxy = _proxy;

#if SAMPLE
        //www-form-urlencodedでPOSTする例
        string postData = "name=ConsoleSoup&nickname=コンソル";
        ProxyNetwork.Post(_url, ProxyNetwork.ContentType.Application_x_www_form_urlencoded, postData, (string json, string error) => {
            //成功の場合は error = null
        });

        他GETと同じくエンコードタイプの指定と、byte[] or stringのコールバックあり
#endif

        //JsonをPOSTする例
        string postData = "{\"name\":\"ConsoleSoup\",\"nickname\":\"コンソル\"}";
        ProxyNetwork.Post(_url, ProxyNetwork.ContentType.Application_json, postData, (string json, string error) => {
            //成功の場合は error = null
        });
    }
}
