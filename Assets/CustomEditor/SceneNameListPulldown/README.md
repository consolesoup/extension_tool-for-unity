SceneNameListPulldown
====

UnityEditorのInspectorにSceneのプルダウンを表示します。  
[Download (unitypackage)](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/master/Assets/CustomEditor/SceneNameListPulldown/SceneNameListPulldown.unitypackage)

- - -

## Description

* **内容物**  
  * SceneNameAttribute.cs (必須)
  * README.md
  * Sample
    * Sample_SceneNameListPulldown.unity
    * Sample_SceneNameListPulldown.cs
  * SceneNameListPulldown.unitypackage

* **使用方法**  

**宣言**  
usingでCustomEditorの利用を宣言する。
``` csharp
using RabitRoad.CustomEditor;
```
<br>

**利用**  
Inspectorに表示されるstringの変数に[SceneName]を付けることで利用可能。    

``` csharp
[SerializeField][SceneName] private string _sceneName;
```

[SceneName(true)]でBuild in Sceneでactive(チェックのついた)なものだけのリストを表示します。

``` csharp
[SerializeField][SceneName(true)] private string _sceneName;
```
<br>

**表示例**  

![SceneNameListPulldown](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/images/SceneNameListPulldown.gif)

※使用例はSampleをご覧ください

* **注意事項**
1. Unityの「BuildSettings > Build in Scene」に追加されているものが自動で選択可能になります。
2. Sceneの名前が変わったり、Build in Sceneから消されたとき、そのSceneを選択していた変数はリストの一番上(Build in Sceneで0番目のScene)のSceneに変更されます。

- - -

## Terms

1. 当データを利用した時点で規約に同意したものとみなします。
2. 当データを改変・改造したデータにも当規約が適用されるものとします。
3. 改造したデータの著作権は当方が所持するものとします。

4. 利用可能
  * 営利利用
  * 改変・改造
  * R-18，R-18G

5. 禁止事項
  * 当データ単体での再配布、販売を禁止します（改造品も含まれます）。
  * ほかの利用者への配布による譲渡を禁止します。（会社、団体の場合はチーム内共有を許可）
  * 自作発言を禁止します（改造品も含まれます）。
  * 公序良俗に反する行為や目的、政治, 宗教活動、他者に対する誹謗中傷目的での利用を禁止します。

6. 免責事項
  * 当データについて当方はいかなる保証も行いません。
  * 当データを利用して何らかのトラブル・損失及び損害等が発生した場合でも当方は一切の責任を負いません。

- - -

## Licence

配布サイトの最新のものを適用する。