TagNameListPulldown
====

UnityEditorのInspectorにタグのプルダウンを表示します。  
[Download (unitypackage)](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/master/Assets/CustomEditor/TagNameListPulldown/TagNameListPulldown.unitypackage)

- - -

## Description

* **内容物**  
  * TagNameAttribute.cs (必須)
  * README.md
  * TagNameList.cs
  * Editor
    * TagNameListCreator.cs
  * Sample
    * Sample_TagNameListPulldown.unity
    * Sample_TagNameListPulldown.cs
  * TagNameListPulldown.unitypackage

* **使用方法**  

**宣言**  
usingでCustomEditorの利用を宣言する。
``` csharp
using RabitRoad.CustomEditor;
```
<br>

**利用**  
Inspectorに表示されるstringの変数に[TagName]を付けることで利用可能。    

``` csharp
[SerializeField][TagName] private string _tagName;
```
<br>

**表示例**

![TagNameListPulldown](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/images/TagNameListPulldown.gif)

※使用例はSampleをご覧ください

* **注意事項**
1. Unityの「Tools > CustomEditor > TagNameListPulldown > Create TagNameList.cs」でタグのリスト(TagNameList.cs)を更新できます。
2. 各変数の値はタグのリストでの番号で管理しています。  タグの順番が変わったり、タグが追加・削除された後、「Create TagNameList.cs」で更新した際に違うタグに変更されることがあります。
3. TagNameListCreator.csにてタグのリストを更新する際にAssets以下固定パスになっているのでフォルダを移動する場合はScript内のPATHを変更してください。

- - -

## Terms

1. 当データを利用した時点で規約に同意したものとみなします。
2. 当データを改変・改造したデータにも当規約が適用されるものとします。
3. 改造したデータの著作権は当方が所持するものとします。

4. 利用可能
  * 営利利用
  * 改変・改造
  * R-18，R-18G

5. 禁止事項
  * 当データ単体での再配布、販売を禁止します（改造品も含まれます）。
  * ほかの利用者への配布による譲渡を禁止します。（会社、団体の場合はチーム内共有を許可）
  * 自作発言を禁止します（改造品も含まれます）。
  * 公序良俗に反する行為や目的、政治, 宗教活動、他者に対する誹謗中傷目的での利用を禁止します。

6. 免責事項
  * 当データについて当方はいかなる保証も行いません。
  * 当データを利用して何らかのトラブル・損失及び損害等が発生した場合でも当方は一切の責任を負いません。

- - -

## Licence

配布サイトの最新のものを適用する。