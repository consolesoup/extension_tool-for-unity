﻿using UnityEngine;

public static class TagNameList
{
    public static string[] GetEnabledTagNames()
    {
        return new string[]
        {
            "Untagged",
            "Respawn",
            "Finish",
            "EditorOnly",
            "MainCamera",
            "Player",
            "GameController"
        };
    }
}
