using System;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace RabitRoad.CustomEditor
{
    public static class TagNameListCreator
    {
        private const string FILENAME = "TagNameList";
        private const string ITEM_NAME = "Tools/CustomEditor/TagNameListPulldown/Create " + FILENAME + ".cs"; // コマンド名
        private const string PATH = "Assets/CustomEditor/TagNameListPulldown/" + FILENAME + ".cs";       // ファイルパス

        [MenuItem(ITEM_NAME)]
        public static void Create()
        {
            if (!CanCreate()) return;

            CreateScript();

            EditorUtility.DisplayDialog(FILENAME, "Create is complete.\n'" + PATH + "'", "OK");
        }

        public static void CreateScript()
        {
            var builder = new StringBuilder();

            builder.AppendLine("using UnityEngine;");
            builder.AppendLine("");
            builder.AppendLine("public static class " + FILENAME);
            builder.AppendLine("{");
            builder.AppendLine("    public static string[] GetEnabledTagNames()");
            builder.AppendLine("    {");

            string tags = "";
            foreach (var n in InternalEditorUtility.tags.Select(c => new { var = RemoveInvalidChars(c), val = c }))
            {
                if (tags != "") tags += ",\n";
                tags += "            \"" + n.val + "\"";
            }
            builder.AppendLine("        return new string[]");
            builder.AppendLine("        {");
            builder.AppendLine(tags);
            builder.AppendLine("        };");
            builder.AppendLine("    }");
            builder.AppendLine("}");

            string directoryName = Path.GetDirectoryName(PATH);
            if (!Directory.Exists(directoryName)) Directory.CreateDirectory(directoryName);

            File.WriteAllText(PATH, builder.ToString(), Encoding.UTF8);
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        [MenuItem(ITEM_NAME, true)]
        public static bool CanCreate()
        {
            return !EditorApplication.isPlaying && !Application.isPlaying && !EditorApplication.isCompiling;
        }

        public static string RemoveInvalidChars(string str)
        {
            string[] invalud_chars =
            {
            " ", "!", "\"", "#", "$",
            "%", "&", "\'", "(", ")",
            "-", "=", "^",  "~", "\\",
            "|", "[", "{",  "@", "`",
            "]", "}", ":",  "*", ";",
            "+", "/", "?",  ".", ">",
            ",", "<"
        };
            Array.ForEach(invalud_chars, c => str = str.Replace(c, string.Empty));
            return str;
        }
    }
}