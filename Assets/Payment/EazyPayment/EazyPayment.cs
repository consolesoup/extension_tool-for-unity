using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

namespace RabitRoad.Payment
{
    public static class EazyPayment
    {
        private static EazyPaymentBehaviour _instance;

        private static string _googlePlayPublicKey;
        public static string GooglePlayPublicKey
        {
            set
            {
                if (_googlePlayPublicKey != value)
                {
                    _googlePlayPublicKey = value;
                    if (_instance != null) _instance.GooglePlayPublicKey = _googlePlayPublicKey;
                }
            }
        }

        private static UnityAction<Product> _appleAskToBuy;
        public static UnityAction<Product> AppleAskToBuy
        {
            set
            {
                if (_appleAskToBuy != value)
                {
                    _appleAskToBuy = value;
                    if (_instance) _instance.AppleAskToBuyCallback = _appleAskToBuy;
                }
            }
        }

        #region Initialize
        public enum InitializeReason
        {
            OK,
            PaymentUnavailable,
            NoProductsAvailable,
            AppNotKnown,
            Unknown
        }

        public static void Initialize(string[] productIds, UnityAction<InitializeReason> callback)
        {
            //instanceの生成
            if (_instance == null)
            {
                GameObject gameObject = new GameObject(typeof(EazyPayment).ToString());
                _instance = gameObject.AddComponent<EazyPaymentBehaviour>();
                _instance.GooglePlayPublicKey = _googlePlayPublicKey;
                _instance.AppleAskToBuyCallback = _appleAskToBuy;
                GameObject.DontDestroyOnLoad(_instance);
            }

            //初期化
            _instance.Initialize(productIds, callback);
        }
        #endregion

        #region Payment
        public enum PaymentReason
        {
            OK,

            PaymentUnavailable,
            ExistingPaymentPending,
            ProductUnavailable,
            SignatureInvalid,
            UserCancelled,
            PaymentDeclined,
            DuplicateTransaction,
            Unknown,

            IncorrectProductID,
            NotInitialized,
            ProductNotFound
        }

        public static void GetProduct(string productId, UnityAction<Product, PaymentReason> callback)
        {
            if (_instance == null) { callback(null, PaymentReason.NotInitialized); return; }

            _instance.GetProduct(productId, callback);
        }

        public static void Payment(Product product, UnityAction<PurchaseEventArgs> success, UnityAction<Product, PaymentReason> failure)
        {
            if (_instance == null) { failure(product, PaymentReason.NotInitialized); return; }

            _instance.Payment(product, success, failure);
        }
        #endregion

        #region ReStore
        public static void ReStore(UnityAction<PurchaseEventArgs> success, System.Action<EazyPayment.PaymentReason> finished)
        {
            if (_instance == null) { finished(PaymentReason.NotInitialized); return; }

            _instance.Restore(success, finished);
        }
        #endregion

        #region Receipt
        private class ReceiptData
        {
            public string Store;
            public string TransactionID;
#if UNITY_IOS
        public string Payload;
#elif UNITY_ANDROID
        public PayloadData Payload;
        [Serializable] public class PayloadData
        {
            public JsonData json;
            public string signature;
            public SKUDetail skuDetails;
            public bool isPurchaseHistorySupported;
        }

        [Serializable] public class JsonData
        {
            public string orderId;
            public string packageName;
            public string productId;
            public long purchaseTime;
            public int purchaseState;
            public DeveloperPayloadData developerPayload;
            public string purchaseToken;
        }
        [Serializable] public class DeveloperPayloadData
        {
            public string developerPayload;
            public bool is_free_trial;
            public bool has_introductory_price_trial;
            public bool is_updated;
            public string accountId;
        }
        [Serializable] public class SKUDetail
        {
            public string productId;
            public string type;
            public string price;
            public long price_amount_micros;
            public string price_currency_code;
            public string title;
            public string description;
        }
#endif
        }

        public static void GetReceiptData(Product product, UnityAction<string, string> callback)
        {
            string receipt = "";
            string signature = "";
            try
            {
                string json = string.IsNullOrEmpty(product.receipt) ? "" : product.receipt;
                json = json.Replace("\\", "");
                json = json.Replace("\"{\"", "{\"");
                json = json.Replace("\"}\"", "\"}");
                json = json.Replace("}\"", "}");
                ReceiptData receiptData = JsonUtility.FromJson<ReceiptData>(json);
#if UNITY_IOS
            if (receiptData != null) receipt = receiptData.Payload;
#elif UNITY_ANDROID
            if (receiptData != null)
            {
                receipt = receiptData.Payload.json.purchaseToken;
                signature = receiptData.Payload.signature;
            }
#endif
            }
            catch { }

            callback(receipt, signature);
        }
        #endregion
    }

    public class EazyPaymentBehaviour : MonoBehaviour, IStoreListener
    {
        #region Initialize
        private IStoreController m_Controller;

        private IAppleExtensions m_AppleExtensions;
        private IMicrosoftExtensions m_MicrosoftExtensions;
        private ITransactionHistoryExtensions m_TransactionHistoryExtensions;
        private IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;

        private string _googlePlayPublicKey = null;
        public string GooglePlayPublicKey { set { _googlePlayPublicKey = value; } }

#pragma warning disable 0414
        private bool m_IsGooglePlayStoreSelected;
#pragma warning restore 0414

        private UnityAction<EazyPayment.InitializeReason> _initializeCallback = null;
        public UnityAction<Product> AppleAskToBuyCallback = null;

        public void Initialize(string[] productIds, UnityAction<EazyPayment.InitializeReason> callback)
        {
            _initializeCallback = callback;

            StandardPurchasingModule module = StandardPurchasingModule.Instance();
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

            ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);

#if UNITY_ANDROID
            builder.Configure<IGooglePlayConfiguration>().SetPublicKey(_googlePlayPublicKey);
#endif

#if UNITY_EDITOR
            builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;
#endif

            m_IsGooglePlayStoreSelected = Application.platform == RuntimePlatform.Android && module.appStore == AppStore.GooglePlay;

            foreach (string productId in productIds)
            {
                if (string.IsNullOrEmpty(productId)) continue;
                builder.AddProduct(productId, ProductType.Consumable);
            }

            UnityPurchasing.Initialize(this, builder);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            m_Controller = controller;

            m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
            m_MicrosoftExtensions = extensions.GetExtension<IMicrosoftExtensions>();
            m_TransactionHistoryExtensions = extensions.GetExtension<ITransactionHistoryExtensions>();
            m_GooglePlayStoreExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();

            m_AppleExtensions.RegisterPurchaseDeferredListener((Product product) =>
            {
                //Ask to Buyの遅延購入
                if (AppleAskToBuyCallback != null) AppleAskToBuyCallback(product);
            });

            _initializeCallback(EazyPayment.InitializeReason.OK);
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    _initializeCallback(EazyPayment.InitializeReason.AppNotKnown);
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    _initializeCallback(EazyPayment.InitializeReason.PaymentUnavailable);
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    _initializeCallback(EazyPayment.InitializeReason.NoProductsAvailable);
                    break;
                default:
                    _initializeCallback(EazyPayment.InitializeReason.Unknown);
                    break;
            }
        }
        #endregion

        #region PaymentCallback

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            //リストア処理
            if (_restoreCallbackData != null)
            {
                _restoreCallbackData.SuccessCallback.Invoke(args);
                _restoreCallbackData.SuccessCallback.RemoveAllListeners();
                _restoreCallbackData = null;
            }

            //購入処理
            if (_paymentCallbackDictionary.ContainsKey(args.purchasedProduct))
            {
                ProductData productData = _paymentCallbackDictionary[args.purchasedProduct];
                productData.Charging = false;
                productData.SuccessCallback.Invoke(args);
                productData.SuccessCallback.RemoveAllListeners();
                productData.FailureCallback.RemoveAllListeners();
                _paymentCallbackDictionary[args.purchasedProduct] = productData;
            }

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            EazyPayment.PaymentReason paymentReason = EazyPayment.PaymentReason.Unknown;
            switch (reason)
            {
                case PurchaseFailureReason.PurchasingUnavailable:
                    paymentReason = EazyPayment.PaymentReason.PaymentUnavailable;
                    break;
                case PurchaseFailureReason.ExistingPurchasePending:
                    paymentReason = EazyPayment.PaymentReason.ExistingPaymentPending;
                    break;
                case PurchaseFailureReason.ProductUnavailable:
                    paymentReason = EazyPayment.PaymentReason.ProductUnavailable;
                    break;
                case PurchaseFailureReason.SignatureInvalid:
                    paymentReason = EazyPayment.PaymentReason.SignatureInvalid;
                    break;
                case PurchaseFailureReason.UserCancelled:
                    paymentReason = EazyPayment.PaymentReason.UserCancelled;
                    break;
                case PurchaseFailureReason.PaymentDeclined:
                    paymentReason = EazyPayment.PaymentReason.PaymentDeclined;
                    break;
                case PurchaseFailureReason.DuplicateTransaction:
                    paymentReason = EazyPayment.PaymentReason.DuplicateTransaction;
                    break;
                case PurchaseFailureReason.Unknown:
                    paymentReason = EazyPayment.PaymentReason.Unknown;
                    break;
            }

            //購入処理
            if (_paymentCallbackDictionary.ContainsKey(product))
            {
                ProductData productData = _paymentCallbackDictionary[product];
                productData.Charging = false;
                productData.FailureCallback.Invoke(product, paymentReason);
                productData.SuccessCallback.RemoveAllListeners();
                productData.FailureCallback.RemoveAllListeners();
                _paymentCallbackDictionary[product] = productData;
            }
        }
        #endregion

        #region Payment
        private class PaymentSuccessCallback : UnityEvent<PurchaseEventArgs> { }
        private class PaymentFailureCallback : UnityEvent<Product, EazyPayment.PaymentReason> { }
        private class ProductData
        {
            public PaymentSuccessCallback SuccessCallback = new PaymentSuccessCallback();
            public PaymentFailureCallback FailureCallback = new PaymentFailureCallback();
            public bool Charging = false;
        }
        private Dictionary<Product, ProductData> _paymentCallbackDictionary = new Dictionary<Product, ProductData>();

        public void GetProduct(string productId, UnityAction<Product, EazyPayment.PaymentReason> callback)
        {
            if (m_Controller == null) { callback(null, EazyPayment.PaymentReason.NotInitialized); return; }

            if (string.IsNullOrEmpty(productId)) { callback(null, EazyPayment.PaymentReason.IncorrectProductID); return; }

            Product product = m_Controller.products.WithID(productId);

            if (product == null) { callback(null, EazyPayment.PaymentReason.ProductNotFound); return; }

            callback(product, EazyPayment.PaymentReason.OK);
        }

        public void Payment(Product product, UnityAction<PurchaseEventArgs> success, UnityAction<Product, EazyPayment.PaymentReason> failure)
        {
            if (m_Controller == null) { failure(null, EazyPayment.PaymentReason.NotInitialized); return; }

            if (product == null) { failure(null, EazyPayment.PaymentReason.ProductNotFound); return; }

#if !UNITY_EDITOR
            if (!product.availableToPurchase) { failure(null, EazyPayment.PaymentReason.ProductUnavailable); return; }
#endif
            ProductData productData = null;

            if (!_paymentCallbackDictionary.ContainsKey(product))
            {
                productData = new ProductData();
                _paymentCallbackDictionary.Add(product, productData);
            }
            else productData = _paymentCallbackDictionary[product];

            productData.SuccessCallback.AddListener(success);
            productData.FailureCallback.AddListener(failure);

            if (!productData.Charging)
            {
                productData.Charging = true;
                m_Controller.InitiatePurchase(product);
            }
        }
        #endregion

        #region ReStore
        private ProductData _restoreCallbackData = null;

        public void Restore(UnityAction<PurchaseEventArgs> success, System.Action<EazyPayment.PaymentReason> finished)
        {
            if (m_Controller == null) { finished(EazyPayment.PaymentReason.NotInitialized); return; }

            if (_restoreCallbackData == null) _restoreCallbackData = new ProductData();

            _restoreCallbackData.SuccessCallback.AddListener(success);

            if (!_restoreCallbackData.Charging)
            {
                _restoreCallbackData.Charging = true;
                if (m_IsGooglePlayStoreSelected) m_GooglePlayStoreExtensions.RestoreTransactions((bool flag) =>
                {
                    if (flag) finished(EazyPayment.PaymentReason.OK);
                    else finished(EazyPayment.PaymentReason.Unknown);
                });
                else m_AppleExtensions.RestoreTransactions((bool flag) =>
                {
                    if (flag) finished(EazyPayment.PaymentReason.OK);
                    else finished(EazyPayment.PaymentReason.Unknown);
                });
            }
        }
        #endregion
    }
}