EazyPayment
====

iOSとAndroidでの課金処理の実装をかんたんにします。  
UnityEditor上では下のGIFにもあるデモ課金でデバッグ可能です。  
非消耗型(Non-consumable)にて動作確認済み。  

内部的にはエラーの内容などそのまま返しているので消耗型(Consumable)、自動更新購読(Auto-renewable subscriptions)、非更新購読(Non-renewable subscriptions)なども利用可能なはず...  
[Download (unitypackage)](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/master/Assets/Payment/EazyPayment/EazyPayment.unitypackage)

- - -

## Description

### 内容物
  * EazyPayment.cs (必須)
  * README.md
  * Sample
    * Sample_EazyPayment.unity
    * Sample_EazyPayment.cs
  * EazyPayment.unitypackage

![EazyPayment](https://gitlab.com/consolesoup/extension_tool-for-unity/raw/images/EazyPayment.gif)

※使用例はSampleをご覧ください

### 使用方法

#### EazyPaymentの説明
EazyPaymentを利用した購入処理、リストアなどの流れを簡略化しました。

【アイテム情報取得】初期化　→　Productの取得  
【購入処理】　　　　初期化　→　Productの取得　→　購入処理    
【リストア処理】　　初期化　→　リストア処理  

上の順番でメソッドを呼ぶだけで実装可能です。

※「初期化」は基本初回1回のみでOK、productIdが追加された場合はリストア以外の処理の前に再度初期化必要です。  
※「Productの取得」も取得後に保持して再利用すれば1回でOKです。

EazyPaymentはUnityEditorでのデモ課金が可能です。（iOSやAndroidのストアには問合せしないデバッグ用の機能）  
環境セットアップでUnityIAPさえあれば動作可能です。

#### Productの説明
Productはストアで作成したアイテムのID = productIdで取得したアイテム情報です。

| 取り出し方                            | 説明                           | 値           |
|:--------------------------------------|:-------------------------------|:-------------|
| product.definition.id                 | productId                      | com.test1    |
| product.metadata.isoCurrencyCode      | 購入者の国に応じた通貨のコード | JPY          |
| product.metadata.localizedTitle       | 購入者の国に応じたアイテム名   | アイテム１   |
| product.metadata.localizedDescription | 購入者の国に応じた説明文       | テストです。 |
| product.metadata.localizedPrice       | 購入者の国に応じた価格の数値   | 100          |
| product.metadata.localizedPriceString | 購入者の国に応じた価格の文字列 | \100         |
| product.availableToPurchase           | 購入可能なProductかどうか      | true         |
| product.transactionID                 | 購入処理の管理ID               | ??????       |
| product.hasReceipt                    | レシート情報があるかどうか     | true         |
| product.receipt                       | 各ストアのレシート情報のjson   | {"Payload":  |

#### 環境セットアップ
##### UnityIAPのImport

PackageManagerにて「In App Purchasing」を追加してください。  
動作確認バージョン： 2.0.6

##### Servicesのセットアップ
[Unity IAP をゲームに統合する](https://unity3d.com/jp/learn/tutorials/topics/ads-analytics/integrating-unity-iap-your-game)の以下の項目を順番にやります。

1. Unity Services のセットアップ
2. Project ID
3. アプリ内購入（In-App Purchasing）を有効にする
4. COPPA への準拠
5. IAP パッケージを追加する

以降はScriptを書く項目なのでEazyPaymentで代行してます。

※エラーが出ている場合はPluginsの中の「UnityPurchasing」「UnityChannel」「UDP」が含まれていない可能性があります。  
同封のunitypackageに含まれてますので上書きしてください。
また、上の3つのフォルダの参照の都合で「Assets/Plugins」以外のパスに移動するとエラーが出ますので注意してください。

##### 課金アイテムの作成

各ストアにてproductId(課金アイテムの購入ID)を作成してください。
よく更新されるのでググってください。

#### EazyPaymentの利用
##### 宣言
usingでUnityIAPとEazyPaymentの利用を宣言する。
``` csharp
using UnityEngine.Purchasing;
using RabitRoad.Payment;
```
<br>

##### 初期化
EazyPaymentを初期化します。  

productIdsにはストアで登録したproductIdを入れます。  
初期化した時のproductIdsに入っているIDのみ購入が可能です。

初期化することでproductIdを元にProduct\[^1]を作成します。

※アイテムリストの通信などでproductIdが追加されたら再度初期化必要
``` csharp
string[] productIds;

//初期化開始
EazyPayment.Initialize(productIds, (EazyPayment.InitializeReason reason) => {
    switch (reason)
    {
        case EazyPayment.InitializeReason.OK:
            //初期化成功
            callback();
            break;
                
        //以下は初期化失敗、本来はポップアップなどでリトライしますか？的なことをやる。
        case EazyPayment.InitializeReason.AppNotKnown:
            //ストアがアプリを認識していない(BundleIDなどが違うとかでストアに登録したアプリの情報と一致していない)
            Initialize(callback);
            break;
        case EazyPayment.InitializeReason.NoProductsAvailable:
            //有効なProductがない
            Initialize(callback);
            break;
        case EazyPayment.InitializeReason.PaymentUnavailable:
            //端末の設定で購入が許可されていない状態
            Initialize(callback);
            break;
        case EazyPayment.InitializeReason.Unknown:
            //不明なエラー
            Initialize(callback);
            break;
    }
});
```
<br>

##### Productの取得
productIdからProductを取得します。
``` csharp
string productId;

//Productの取得
EazyPayment.GetProduct(productId, (Product product, EazyPayment.PaymentReason reason) => {
    switch (reason)
    {
        case EazyPayment.PaymentReason.OK:
            {
                //Productの取得に成功
                【取得成功後の処理】　※キャッシュするなりUI更新するなり
            }
            break;
        case EazyPayment.PaymentReason.ProductNotFound:
            //Productが見つかりません。
            break;
        case EazyPayment.PaymentReason.IncorrectProductID:
            //不正なproductIdです。
            break;
        case EazyPayment.PaymentReason.NotInitialized:
            {
                //初期化して再度Product取得処理
            }
            break;

        //以下は呼ばれません。
        case EazyPayment.PaymentReason.PaymentUnavailable:
        case EazyPayment.PaymentReason.ExistingPaymentPending:
        case EazyPayment.PaymentReason.ProductUnavailable:
        case EazyPayment.PaymentReason.SignatureInvalid:
        case EazyPayment.PaymentReason.UserCancelled:
        case EazyPayment.PaymentReason.PaymentDeclined:
        case EazyPayment.PaymentReason.DuplicateTransaction:
        case EazyPayment.PaymentReason.Unknown:
            break;
    }
});
```
<br>

##### 購入処理
EazyPaymentでは購入時にProductが必要です。
``` csharp
Product product; //Productの取得で得たもの

//購入前通信とかやるならここ
//購入限度数とかチェックしたり...

//Productの購入
EazyPayment.Payment(product, (PurchaseEventArgs args) => {
    //Productの取得に成功
    //「 args.purchasedProduct 」で購入に使用したProductを取得できます。
    【レシートチェックの処理などを行う】
}, (Product _product, EazyPayment.PaymentReason reason) => {
    //Productの取得に失敗
    switch (reason)
    {
        case EazyPayment.PaymentReason.ExistingPaymentPending:
            //購入処理中
            break;
        case EazyPayment.PaymentReason.SignatureInvalid:
            //不正な購入情報
            break;
        case EazyPayment.PaymentReason.UserCancelled:
            //購入がユーザーによって拒否されました
            break;
        case EazyPayment.PaymentReason.PaymentDeclined:
            //支払いで失敗しました
            break;
        case EazyPayment.PaymentReason.DuplicateTransaction:
            //前回の処理が完了していません。
            break;
        case EazyPayment.PaymentReason.Unknown:
            //未知のエラー;
            break;
        case EazyPayment.PaymentReason.ProductUnavailable:
            //購入できないアイテムです。
            break;
        case EazyPayment.PaymentReason.ProductNotFound:
            //Productが見つかりません。
            break;
        case EazyPayment.PaymentReason.PaymentUnavailable:
            //この端末では購入することができません。
            break;
        case EazyPayment.PaymentReason.NotInitialized:
            {
                //EazyPaymentが初期化されていません。
                【初期化して再度購入処理】
            }
            break;

        //以下は呼ばれません。
        case EazyPayment.PaymentReason.OK:
        case EazyPayment.PaymentReason.IncorrectProductID:
            break;
    }
});
```
<br>

##### リストア処理
リストアには初期化のみ必要です。
``` csharp
EazyPayment.ReStore((PurchaseEventArgs args) => {
    //購入済みのアイテムが来る（購入処理成功時と同じデータ）
    //「 args.purchasedProduct 」で購入に使用したProductを取得できます。
    【レシートチェックの処理などを行う】
}, (EazyPayment.PaymentReason reason) => {
    switch (reason)
    {
        case EazyPayment.PaymentReason.OK:
            //リストア成功
            break;
        case EazyPayment.PaymentReason.Unknown:
            //リストア失敗
            break;
        case EazyPayment.PaymentReason.NotInitialized:
            {
                //EazyPaymentが初期化されていない
                【初期化して再度リストア処理】
            }
            break;

        //以下は呼ばれません。
        case EazyPayment.PaymentReason.PaymentUnavailable:
        case EazyPayment.PaymentReason.ExistingPaymentPending:
        case EazyPayment.PaymentReason.ProductUnavailable:
        case EazyPayment.PaymentReason.SignatureInvalid:
        case EazyPayment.PaymentReason.UserCancelled:
        case EazyPayment.PaymentReason.PaymentDeclined:
        case EazyPayment.PaymentReason.DuplicateTransaction:
        case EazyPayment.PaymentReason.ProductNotFound:
        case EazyPayment.PaymentReason.IncorrectProductID:
            break;
    }
});
```
<br>

##### レシートチェック
購入処理やリストア処理の成功時に呼び出す想定
``` csharp
Product product; //購入に成功したProduct

//レシート情報の取得
EazyPayment.GetReceiptData(product, (string receipt, string signature) => {
    //iOSはreceiptのみでレシートチェックできます。
    //Androidはreceiptとsignatureでレシートチェックできます。

    //購入後通信（サーバーでのレシートチェックなど）
    //receiptは長い文字列なのでPOSTで送信する

    //解放処理（productIdなどに応じて解放フラグをゴニョゴニョ）
});
```
- - -

## Terms

1. 当データを利用した時点で規約に同意したものとみなします。
2. 当データを改変・改造したデータにも当規約が適用されるものとします。
3. 改造したデータの著作権は当方が所持するものとします。

4. 利用可能
  * 営利利用
  * 改変・改造
  * R-18，R-18G

5. 禁止事項
  * 当データ単体での再配布、販売を禁止します（改造品も含まれます）。
  * ほかの利用者への配布による譲渡を禁止します。（会社、団体の場合はチーム内共有を許可）
  * 自作発言を禁止します（改造品も含まれます）。
  * 公序良俗に反する行為や目的、政治, 宗教活動、他者に対する誹謗中傷目的での利用を禁止します。

6. 免責事項
  * 当データについて当方はいかなる保証も行いません。
  * 当データを利用して何らかのトラブル・損失及び損害等が発生した場合でも当方は一切の責任を負いません。

- - -

## Licence

配布サイトの最新のものを適用する。