using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using RabitRoad.Payment;

public class Sample_EazyPayment : MonoBehaviour
{
    [SerializeField] private Text _logText;
    [SerializeField] private string _productId;

    // Start is called before the first frame update
    void Start()
    {
        //EazyPayment 初期化
        Initialize(() => { });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Initialize(UnityAction callback)
    {
        _logText.text = "初期化開始";
        EazyPayment.Initialize(new string[]{_productId}, (EazyPayment.InitializeReason reason) => {
            switch (reason)
            {
                case EazyPayment.InitializeReason.OK:
                    _logText.text = "初期化成功";
                    callback();
                    break;
                
                //本来はポップアップなどでリトライしますか？的なことをやる。
                case EazyPayment.InitializeReason.AppNotKnown:
                    _logText.text = "初期化失敗("+reason.ToString()+")";
                    Initialize(callback);
                    break;
                case EazyPayment.InitializeReason.NoProductsAvailable:
                    _logText.text = "初期化失敗("+reason.ToString()+")";
                    Initialize(callback);
                    break;
                case EazyPayment.InitializeReason.PaymentUnavailable:
                    _logText.text = "初期化失敗("+reason.ToString()+")";
                    Initialize(callback);
                    break;
                case EazyPayment.InitializeReason.Unknown:
                    _logText.text = "初期化失敗("+reason.ToString()+")";
                    Initialize(callback);
                    break;
            }
        });
    }

    public void Payment()
    {
        //購入前通信（サーバーに問い合わせなど）

        //Productの取得
        EazyPayment.GetProduct(_productId, (Product product, EazyPayment.PaymentReason reason) => {
            switch (reason)
            {
                case EazyPayment.PaymentReason.OK:
                    {
                        _logText.text = product.metadata.localizedTitle + " (" + product.metadata.localizedPriceString + ") の情報の取得に成功、購入処理開始";
                        _paymentProduct(product);
                    }
                    break;
                case EazyPayment.PaymentReason.ProductNotFound:
                    _logText.text = "Productが見つかりません。";
                    break;
                case EazyPayment.PaymentReason.IncorrectProductID:
                    _logText.text = "不正なproductIdです。";
                    break;
                case EazyPayment.PaymentReason.NotInitialized:
                    {
                        _logText.text = "EazyPaymentが初期化されていません。";
                        Initialize(Payment); //初期化して再度購入処理
                    }
                    break;

                //以下は呼ばれません。
                case EazyPayment.PaymentReason.PaymentUnavailable:
                case EazyPayment.PaymentReason.ExistingPaymentPending:
                case EazyPayment.PaymentReason.ProductUnavailable:
                case EazyPayment.PaymentReason.SignatureInvalid:
                case EazyPayment.PaymentReason.UserCancelled:
                case EazyPayment.PaymentReason.PaymentDeclined:
                case EazyPayment.PaymentReason.DuplicateTransaction:
                case EazyPayment.PaymentReason.Unknown:
                    break;
            }
        });
    }

    private void _paymentProduct(Product product)
    {
        //Productの購入
        EazyPayment.Payment(product, (PurchaseEventArgs args) => {
            //Productの取得に成功
            _logText.text = args.purchasedProduct.metadata.localizedTitle + " (" + args.purchasedProduct.metadata.localizedPriceString + ") の購入に成功";
            _paymentedProductRelease(args.purchasedProduct);
        }, (Product _product, EazyPayment.PaymentReason reason) => {
            //Productの取得に失敗
            switch (reason)
            {
                case EazyPayment.PaymentReason.OK:
                    {
                        _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") の購入に成功しました。";
                        _paymentedProductRelease(_product);
                    }
                    break;
                case EazyPayment.PaymentReason.ExistingPaymentPending:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") は購入処理中です。";
                    break;
                case EazyPayment.PaymentReason.SignatureInvalid:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") は不正な購入情報です。";
                    break;
                case EazyPayment.PaymentReason.UserCancelled:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") の購入がユーザーによって拒否されました。";
                    break;
                case EazyPayment.PaymentReason.PaymentDeclined:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") の支払いで失敗しました。";
                    break;
                case EazyPayment.PaymentReason.DuplicateTransaction:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") の前回の処理が完了していません。";
                    break;
                case EazyPayment.PaymentReason.Unknown:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") で未知のエラー";
                    break;
                case EazyPayment.PaymentReason.ProductUnavailable:
                    _logText.text = _product.metadata.localizedTitle + " (" + _product.metadata.localizedPriceString + ") は購入できないアイテムです。";
                    break;
                case EazyPayment.PaymentReason.ProductNotFound:
                    _logText.text = "Productが見つかりません。";
                    break;
                case EazyPayment.PaymentReason.PaymentUnavailable:
                    _logText.text = "この端末では購入することができません。";
                    break;
                case EazyPayment.PaymentReason.NotInitialized:
                    {
                        _logText.text = "EazyPaymentが初期化されていません。";
                        Initialize(Payment); //初期化して再度購入処理
                    }
                    break;

                //以下は呼ばれません。
                case EazyPayment.PaymentReason.IncorrectProductID:
                    break;
            }
        });
    }

    public void ReStore()
    {
        EazyPayment.ReStore((PurchaseEventArgs args) => {
            //購入済みのアイテムが来る
            _logText.text = args.purchasedProduct.metadata.localizedTitle + " (" + args.purchasedProduct.metadata.localizedPriceString + ") がリストア";
            _paymentedProductRelease(args.purchasedProduct);
        }, (EazyPayment.PaymentReason reason) => {
            switch (reason)
            {
                case EazyPayment.PaymentReason.OK:
                    _logText.text = "リストア成功";
                    break;
                case EazyPayment.PaymentReason.Unknown:
                    _logText.text = "リストア失敗";
                    break;
                case EazyPayment.PaymentReason.NotInitialized:
                    {
                        _logText.text = "EazyPaymentが初期化されていません。";
                        Initialize(Payment); //初期化して再度購入処理
                    }
                    break;

                //以下は呼ばれません。
                case EazyPayment.PaymentReason.PaymentUnavailable:
                case EazyPayment.PaymentReason.ExistingPaymentPending:
                case EazyPayment.PaymentReason.ProductUnavailable:
                case EazyPayment.PaymentReason.SignatureInvalid:
                case EazyPayment.PaymentReason.UserCancelled:
                case EazyPayment.PaymentReason.PaymentDeclined:
                case EazyPayment.PaymentReason.DuplicateTransaction:
                case EazyPayment.PaymentReason.ProductNotFound:
                case EazyPayment.PaymentReason.IncorrectProductID:
                    break;
            }
        });
    }

    private void _paymentedProductRelease(Product product)
    {
        _logText.text = product.metadata.localizedTitle + " (" + product.metadata.localizedPriceString + ") のレシート取得開始";
        EazyPayment.GetReceiptData(product, (string receipt, string signature) => {
            _logText.text = product.metadata.localizedTitle + " (" + product.metadata.localizedPriceString + ") のレシートとシグネチャの取得終了";

            //購入後通信（レシートチェックなど）

            //解放処理（productIdなどに応じて解放フラグをゴニョゴニョ）
        });
    }
}
